JM.SB_main = (function(){

    var homeTransform = function() {

    };

    var viewAllTransform = function() {
        $('.sold_out_tag').each(function(index, el) {
            $(this).closest('article').addClass('SB_st-soldout');
        });
        $('.sidebar').addClass('SB_st-visible');
    };

    var changeLink = function() {
        var $shoplink = $('.shop_link','body');
        $shoplink.attr('href', '/shop/all');
    };

    $('#sb_hide-so').on('click', function(event) {
        if ($('#hide_sold_out').is(':checked')) {
            chrome.storage.local.set({hide_so: 'false'});
        } else {
            chrome.storage.local.set({hide_so: 'true'});
        }
    });

    var callback = function() {

        chrome.storage.local.get('hide_so', function (result) {
            hideso = result.hide_so;
            if (hideso == 'true') {
                $('body').addClass('SB_so-hide');
            }
        });

        $('body').addClass('supreme_sb');

        if(window.location.href.indexOf("shop/shipping") > -1) {
            $('body').addClass('shipping');
        }

        if(window.location.href.indexOf("shop/terms") > -1) {
            $('body').addClass('terms');
        }

        if(window.location.href.indexOf("shop/faq") > -1) {
            $('body').addClass('faq');
        }

        if(window.location.href.indexOf("shop/sizing") > -1) {
            $('body').addClass('sizing');
        }

        if ($('body').hasClass('home')) {
            homeTransform();
        }

        if ($('body').hasClass('view-all')) {
            viewAllTransform();
        }

    };

    chrome.storage.local.get('hide_so', function (result) {
        hideso = result.hide_so;
        if (hideso == 'true') {
            $('#hide_sold_out').prop('checked', true);
        } else {
            $('.hide_sold_out').prop('checked', false);
        }
    });

    // chrome.storage.local.set({toggle: 'on'}, function() {
    //     chrome.browserAction.setIcon({path: "/htdocs/assets/img/ci/ext-icon.png"});
    //     // chrome.tabs.executeScript({file:"/htdocs/assets/js/content.js"});
    // });


    $(document).bind("DOMSubtreeModified", 'nav', function(event) {
        changeLink();
        viewAllTransform();

        chrome.storage.local.get('hide_so', function (result) {
            hideso = result.hide_so;
            if (hideso == 'true') {
                $('body').addClass('SB_so-hide');
            }
        });
    });


    if($(document).find("title").text().indexOf("Supreme") > -1) {
        callback();
    }

    return {
        callback: callback,
        changeLink: changeLink
    };


}());



