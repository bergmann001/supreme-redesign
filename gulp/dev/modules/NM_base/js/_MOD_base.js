/**
* @desc:
* init singleton base-class (connect to backend controller via JM = JMG.getPrivateNs();
*/
JM.register('base', [':load'], function(){
	var base = function(config){
		//---------------------------------------------------------
		//define __construct
		constructor : JM.base;
		//---------------------------------------------------------
		//__construct object
		JM.base = function(){
			return(_instance);
		}
		//---------------------------------------------------------
		//internal private singleton object returned with new
		var _instance = {
			//-----------------------------------------------------
			//membervars (internal params)
			//-----------------------------------------------------
			/**
			 * @desc:
			 * member vars
			 */
			_params:{},
			//-----------------------------------------------------
			//class methods
			//-----------------------------------------------------
			/**
			* @desc:
			* check if param exists
			*/
			isset: function(varName){
				if(this._params[varName] && this._params[varName] !== 'undefined'){
					return true;
				}
				return false;
			},
			/**
			* @desc:
			* sets param (or a object)
			*/
			set: function(varName,varData){
				if(typeof varName !== 'undefined'){
					if(typeof varName === 'object'){
						for(var key in varName){
							this._params[key] =  varName[key];
						}
					}
					else{
						this._params[varName] = varData;
					}
				}
			return this;
			},
			/**
			* @desc
			* helper returns param
			*
			* @input-required
			* @var varName {object}
			*
			* @input-optional
			* @var varName {object}
			*
			* @return {int}
			*/
			get: function(varName, _default){
				if(typeof varName === 'undefined'){
					return this._params
				}
				else if(typeof this._params[varName] !== 'undefined'){
					return this._params[varName];
				}
				else if(typeof this._params[varName] === 'undefined' && typeof _default !== 'undefined'){
					return _default;	
				}
			},
			/**
			* @desc
			* helper counts objects
			*
			* @input-required
			* @var obj {object}
			*
			* @return {int}
			*/
			count: function(obj){
				var _size = 0;
				if(typeof obj == 'object'){
					for(var key in obj) {
						if (obj.hasOwnProperty(key)){
							_size++;
						}
					}
				}
				return _size;
			},
			/**
			* @desc
			* helper checks if array/object the the passed value objects
			*
			* @input-required
			* @var needle {string}
			* @var haystack {object}
			*
			* @return {int}
			*/
			inObject: function(needle, haystack) {
				if(typeof haystack == 'object'){
					for(var i in haystack){
						if(haystack[i] === needle) return true;		
					}
				}
				else{
					var length = haystack.length;
					for(var i = 0; i < length; i++){
						if(typeof haystack == 'object'){
							alert(haystack[i]);	
						}
						if(haystack[i] === needle) return true;
					}
				}
				return false;
			}
		}
		//---------------------------------------------------------
		//init with config on first call
		_instance.set(config);
		//-------------------------------------------------------------
	return(_instance);
	};
	//init base
	return new base();
});


