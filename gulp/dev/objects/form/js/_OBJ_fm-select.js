JM.OBJ_fm_select = (function(){
    var callback = function (context) {
        if (context === undefined) {
            context = null;
        }
        if ($('.JM_js-customselect', context).length) {
            $('.JM_js-customselect').selectOrDie({
                customClass: "JM_OBJ_fm-select-generated"
            });
        }
    }
    callback();
    return {
        callback:callback
    }
}());