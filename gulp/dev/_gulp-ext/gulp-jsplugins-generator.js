var fs = require('fs');

module.exports = function(){
    this.jsRegistry = null;

    this.useRegistry= function(_jsRegistry) {
        this.jsRegistry = _jsRegistry;
    };

    this.dissolvePluginList = function(arrPluginList, sourceRegistry) {
        var resultingList = [];
        for (var i in arrPluginList) {
            var p               = arrPluginList[i];


            var pName           = p['plugin'];
            p['bStandalone']    = !!p['bStandalone'];

            if (!sourceRegistry[pName]) {
                console.error('`'+pName + '` could not be resolved as existing plugin');
                process.exit(1);
            }


            if (sourceRegistry[pName].isPackage) {
                var inplacePluginsForPackage = sourceRegistry[pName].set;
                for (var pIndex in inplacePluginsForPackage) {
                    resultingList.push({
                        "bStandalone": p['bStandalone'] || false,
                        "sStandaloneRootDir": null || p['sStandaloneRootDir'],
                        "plugin": inplacePluginsForPackage[pIndex]
                    });
                }
            } else {
                resultingList.push(p);
            }
        }

        return resultingList;
    };

    this.generate = function(arrPluginList, verifyExistenceInPath) {
        if (this.jsRegistry==null) {
            throw new Error('generate() cannot be called when the registry was not setup in advance!');
            return false;
        }


        var jsplugins = this.jsRegistry;
        var jsPluginDefinitions = [];
        var dissolvedArrPluginList = this.dissolvePluginList(arrPluginList, jsplugins);



        for (var i in dissolvedArrPluginList) {
            var plugin = dissolvedArrPluginList[i];
            var pluginName = plugin["plugin"];
            var pluginStdTargetPath = plugin["sStandaloneRootDir"];

            if (!jsplugins[pluginName]) {
                return {
                    error: true,
                    errorMessage: 'The following plugin is not available in the definitions registry: '+pluginName
                };
            } else {
                var dirName     = jsplugins[pluginName].dirName;
                var fileName    = jsplugins[pluginName].fileName;
                jsplugins[pluginName].bStandalone = plugin["bStandalone"];
                if (jsplugins[pluginName].bStandalone) {
                    jsplugins[pluginName].sStandaloneRootDir = pluginStdTargetPath;
                }
                jsplugins[pluginName].path = dirName + '/' + fileName;
                jsplugins[pluginName].basePath = dirName + '/';

                if (verifyExistenceInPath) {

                    var pathToCheck =
                        verifyExistenceInPath+'/'+jsplugins[pluginName].path;
                    if (!fs.existsSync(pathToCheck)) {
                        return {
                            error: true,
                            errorMessage:
                                'The following path could not be verified:'+
                                pathToCheck
                        };
                    } else {
                        jsplugins[pluginName].fullPath = pathToCheck;
                        jsplugins[pluginName].fullBasePath = verifyExistenceInPath+'/'+jsplugins[pluginName].basePath;
                    }
                }

                jsPluginDefinitions.push(jsplugins[pluginName]);
            }
        }

        return {
            error: false,
            jsPluginDefinitions: jsPluginDefinitions
        }
    };
};
