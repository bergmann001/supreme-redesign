module.exports = function(regKey, valKey, endFunc) {
    this.__registry = [];
    this.__reservedKeys = {};
    
    
    var regKey;
    if (!regKey) {
        regKey = 'search';
    }
    
    this.reset = function() {
        this.__registry = [];
        this.__reservedKeys = {};
        
        return this;
    };
    
    this.push = function(data) {
        if (!(regKey in data)) {
            throw new Error('Your registry data must contain the key "'+regKey+'"');
        }
        
        if (!(valKey in data)) {
            throw new Error('Your registry data must contain the key "'+valKey+'" to be able to assign the value');
        }
        
        if (this.__reservedKeys[data[regKey]]) {
            throw new Error('"'+data[regKey]+'" is already allocated/given. Avoid non-unique names...!');
        }
        
        this.__reservedKeys[data[regKey]] = true; //reserving this :)
        this.__registry.push(data);
        
        return this;
    };
    
    this.fetch = function() {
        return [].concat(this.__registry);
    };
    
    //this function will add the registry convential to the preprocessing-context object
    this.addToPpContextObj = function(contextObj) {
        //dont use contextObj.context here as we always assume that we are INSIDE the context
        for (var i in this.__registry) {
            contextObj[this.__registry[i][regKey]] = this.__registry[i][valKey];
        }
        
        return this;
    };
    
    this.end = function() {
        if (!endFunc) {
            console.log('end() was called on registry but no endFunc was defined... Returning self.');
            return this;
        }
        return endFunc();
    };
};