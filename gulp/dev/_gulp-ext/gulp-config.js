var pluginGeneratorHelper = require('./gulp-jsplugins-generator.js');
var dataRegistry = require('./data-registry.js');

function strNormalizePath(strPath) {
    return strPath.replace(/\\/g,'/');
}


module.exports = {
    targetBuildPath: {
        root: null,
        assets: 'assets',
        imageAssets: 'img'
    },
    jsPlugins: {
        "use": [],
        _generated: {
            error: true,
            errorMessage: 'not even generated!',
            jsPluginDefinitions: []
        },
        Generator: new pluginGeneratorHelper()
    },
    _sourceDevPath: null, //unset at init
    _sourceDevSubPathPlugins: 'plugins',
    _localVars: {},
    _fileVariables: [],
    _tplUsablesRegistry: null,
    _inclusionProcessorFunc: false,


    _paths: {
        dev: {
            root: 'dev',
            subPath_jsPluginsSrc: 'plugins',
            subPath_assets: 'assets',
            assetsSubPath_imgSrc: 'img',
            subPath_modulesSrc: 'modules',
            subPath_objectsSrc: 'objects'
        },
        build: {
            root: 'app',
            subPath_assets: 'docroot/assets',
            assetsSubPath_images: 'img'
        }
    },


    /**
    This function checks if the required conf data is provided
    **/
    verify: function() {
        if (!this._paths.build.root) {
            throw new Error('Please provide a build path with buildAt()');
        }
        /*if (this.targetBuildPath.root==null) {

        }*/

        if (!this._paths.dev.root) {
            throw new Error('Please provide a source dev path with devSource()');
        }
        /*
        if (this._sourceDevPath==null) {

        }*/

        this.jsPlugins._generated = this.jsPlugins.Generator.generate(
            this.jsPlugins["use"],
            this._paths.dev.root + '/assets/js/'+this._paths.dev.subPath_jsPluginsSrc
        );

        if (this.jsPlugins._generated.error == true) {
            console.error(this.jsPlugins._generated.errorMessage);
            throw new Error('jsPlugins could not be verified');
        }
    },

    print: function(bDeep) {
        console.log('#----------------');
        console.log('#------------------------');
        console.log('BuildConfig Status information:');
        console.log('> [dev-path]: '+ this._paths.dev.root);
        console.log('> [build-path]: '+ this._paths.build.root);
        console.log('> [build-assets-path]: '+ this._paths.build.subPath_assets);

        var jspdef = [];
        if (this.jsPlugins._generated.error==false) {
            jspdef = this.jsPlugins._generated.jsPluginDefinitions;
        }

        if (jspdef.length>0) {
            console.log('> [JS Plugins in use] ('+jspdef.length+'): ');

            for (var p in jspdef) {
                console.log("\t "+jspdef[p].fullPath);
            }
        }

        var _fileVarsContext = [];
        var _fvcStr = 'user-only';
        if (!bDeep) {
            _fileVarsContext = _fileVarsContext.concat(this._fileVariables);
        } else {
            _fvcStr = 'all';
            _fileVarsContext = this.createPreprocessContext({includeJsPlugins: true, _asArray: true});
        }

        if (_fileVarsContext.length>0) {

            console.log('> [File Variables] ('+_fileVarsContext.length+'): ');
            for (var sRep in _fileVarsContext) {
                console.log("\t- "+_fileVarsContext[sRep]['search']
                            + "\n\t\t"
                            +''+_fileVarsContext[sRep]['replace']+''
                           +"\n");
            }
        } else {
            console.log('> [File Variables]: none');
        }



        console.log('#------------------------');
        console.log('#----------------');
        console.log(" \n");
    },

    buildAt: function(path) {
        //this.targetBuildPath.root = path;
        this._paths.build.root = path;

        return this.__buildPathsetClosure('build', {
            'assetsAt': 'subPath_assets',
            'assets_imagesAt': 'assetsSubPath_images'
        });
    },

    devFrom: function(path) {
        this._paths.dev.root = path;

        return this.__buildPathsetClosure('dev', {
            'assetsFrom': 'subPath_assets',
            'assets_imagesFrom': 'assetsSubPath_imgSrc',
            'modulesFrom': 'subPath_modulesSrc',
            'objectsFrom': 'subPath_objectsSrc'
        });
    },

    __buildPathsetClosure: function(type, ops) {
        var _this = this;

        var closure = {
            'end': function() {
                return _this;
            }
        };

        for (var i in ops) {
            closure[i] = (function(pathkeyName, type){
                return function(path) {
                    _this._paths[type][pathkeyName] = path;
                    return this;
                };
            }(ops[i], type));
        }

        return closure;
    },

    jsplugins: function() {
        var _this = this;
        return {
            end: function() {
                return _this;
            },
            add: function(plugins, bAsStandalone, standaloneRootDir) {
                if ((typeof plugins).toLowerCase()=='string') {
                    plugins = [plugins];
                }

                for (var i in plugins) {
                    if (plugins[i].isPackage) {
                        //is not a plugin but a collection of plugins
                        this.add(plugins[i].set, bAsStandalone, standaloneRootDir);
                        continue;
                    }

                    if (_this.jsPlugins["use"].indexOf(plugins[i]) < 0) {
                        _this.jsPlugins["use"].push({
                            bStandalone: !!bAsStandalone,
                            sStandaloneRootDir: standaloneRootDir ? standaloneRootDir : 'standalone',
                            plugin: plugins[i]
                        });
                    }
                }


                return _this.jsplugins();
            },
            useJsRegistry: function(jsRegistry) {
                _this.jsPlugins.Generator.useRegistry(jsRegistry);
                return _this.jsplugins();
            }
        }
    },

    fileVariables: function(fileVariablesSet) {
        this._fileVariables = fileVariablesSet;
        /**
        valid syntax:
        [
            {search: 'TOSEARCHFOR': replace: 'ELO!'}
        ]
        **/
        return this;
    },

    localVar: function(key, optVal) {
        if (optVal!==undefined) {
            this._localVars[key] = optVal;
        } else {
            if (this._localVars[key]!==undefined) {
                return this._localVars[key];
            } else {
                return null;
            }
        }

        return this;
    },


    // NOW ONLY GETTERS

    getDevImgDirname: function() {
        return this._paths.dev.assetsSubPath_imgSrc;
    },

    getDevSrcImgPath: function() {
        return this.getDevPath(this._paths.dev.subPath_assets+'/'+this._paths.dev.assetsSubPath_imgSrc);
    },

    getDevPath: function(subPath) {
        if (!subPath) {
            return this._paths.dev.root;
        } else {
            return this._paths.dev.root + '/' + subPath;
        }
    },

    getDevAssetsPath: function(subPath) {
        return this.getDevPath(this._paths.dev.subPath_assets+(subPath ? '/'+subPath : ''));
    },

    getBuildPath: function(subPath) {
        if (!subPath) {
            return this._paths.build.root;
        } else {
            return this._paths.build.root+ '/' + subPath;
        }
    },

    getTargetAssetsPath: function(subPath) {
        return this.getBuildPath(this._paths.build.subPath_assets+ (!subPath ? '' : '/'+subPath));
    },

    getTargetCssPath: function() {
        return this.getTargetAssetsPath('css');
    },

    getTargetJsPath: function() {
        return this.getTargetAssetsPath('js');
    },

    getTargetImgPath: function() {
        return this.getTargetAssetsPath(this._paths.build.assetsSubPath_images);
    },

    getTargetTemplatesPath: function() {
        return this._paths.build.root + '/templates';
    },

    getJsPluginsInclusionString: function() {
        var str = '';

        for (var i=0; i<this.jsPlugins._generated.jsPluginDefinitions.length; i++) {
            var jsPluginDef = this.jsPlugins._generated.jsPluginDefinitions[i];

            if (jsPluginDef.bStandalone) {
                //is not compiled into custom file!!, so just continue the loop!
                continue;
            }

            str += '//= include ./'+this._paths.dev.subPath_jsPluginsSrc+'/'+jsPluginDef.path + "\n";
        }


        return str;
    },

    getStandaloneJsPlugins: function() {
        var paths = [];
        var plugin2TargetPathMap = {};

        for (var i=0; i<this.jsPlugins._generated.jsPluginDefinitions.length; i++) {
            var jsPluginDef = this.jsPlugins._generated.jsPluginDefinitions[i];


            if (!jsPluginDef.bStandalone) {
                continue;

            }

            paths.push(strNormalizePath(jsPluginDef.fullPath));
            plugin2TargetPathMap[strNormalizePath(jsPluginDef.fullBasePath)] = jsPluginDef;
        }

        var _this = this;
        return {
            "paths": paths,
            "dest": function(srcVinylFile) {
                var pathMapKey = strNormalizePath('.'+srcVinylFile.base.replace(srcVinylFile.cwd,''));
                return _this.getTargetJsPath()+'/'+plugin2TargetPathMap[pathMapKey].sStandaloneRootDir;
            }
        };
    },

    createPreprocessContext: function(opts) {
        var contextMap = {};

        if (opts && opts.includeJsPlugins && opts.includeJsPlugins===true) {
            contextMap.PLUGINS_JS_INCLUSION_LIST = this.getJsPluginsInclusionString();
        }

        if (opts && opts.includeTplUsables && opts.includeTplUsables===true) {
            this.tplUsablesRegistry().addToPpContextObj(contextMap);
        }

        for (var objKey in this._fileVariables) {
            var searchAndReplace = this._fileVariables[objKey];
            contextMap[searchAndReplace.search] = searchAndReplace.replace;
        }

        if (opts._asArray) {
            var arr = [];
            for (var strKey in contextMap) {
                arr.push({search: strKey, replace: contextMap[strKey]});
            }

            return arr;
        }

        var c = {"context": contextMap, "ignoreInclude": true};

        if (this._inclusionProcessorFunc) {
            c.inclusionProcessor = this._inclusionProcessorFunc;
        }

        if (opts.noNgExpressionReplacement) {
            c.noNgExpressionReplacement = true;
        }

        return c;
    },

    inclusionFilter: function(inclusionProcessorFunc) {
        this._inclusionProcessorFunc = inclusionProcessorFunc;
        return this;
    }
};
