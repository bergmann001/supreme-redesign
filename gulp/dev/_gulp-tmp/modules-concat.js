JM.NM_1_0_header = (function(){



}());


JM.NM_4_0_footer = (function(){
	
}());
/**
* @desc:
* init singleton base-class (connect to backend controller via JM = JMG.getPrivateNs();
*/
JM.register('base', [':load'], function(){
	var base = function(config){
		//---------------------------------------------------------
		//define __construct
		constructor : JM.base;
		//---------------------------------------------------------
		//__construct object
		JM.base = function(){
			return(_instance);
		}
		//---------------------------------------------------------
		//internal private singleton object returned with new
		var _instance = {
			//-----------------------------------------------------
			//membervars (internal params)
			//-----------------------------------------------------
			/**
			 * @desc:
			 * member vars
			 */
			_params:{},
			//-----------------------------------------------------
			//class methods
			//-----------------------------------------------------
			/**
			* @desc:
			* check if param exists
			*/
			isset: function(varName){
				if(this._params[varName] && this._params[varName] !== 'undefined'){
					return true;
				}
				return false;
			},
			/**
			* @desc:
			* sets param (or a object)
			*/
			set: function(varName,varData){
				if(typeof varName !== 'undefined'){
					if(typeof varName === 'object'){
						for(var key in varName){
							this._params[key] =  varName[key];
						}
					}
					else{
						this._params[varName] = varData;
					}
				}
			return this;
			},
			/**
			* @desc
			* helper returns param
			*
			* @input-required
			* @var varName {object}
			*
			* @input-optional
			* @var varName {object}
			*
			* @return {int}
			*/
			get: function(varName, _default){
				if(typeof varName === 'undefined'){
					return this._params
				}
				else if(typeof this._params[varName] !== 'undefined'){
					return this._params[varName];
				}
				else if(typeof this._params[varName] === 'undefined' && typeof _default !== 'undefined'){
					return _default;	
				}
			},
			/**
			* @desc
			* helper counts objects
			*
			* @input-required
			* @var obj {object}
			*
			* @return {int}
			*/
			count: function(obj){
				var _size = 0;
				if(typeof obj == 'object'){
					for(var key in obj) {
						if (obj.hasOwnProperty(key)){
							_size++;
						}
					}
				}
				return _size;
			},
			/**
			* @desc
			* helper checks if array/object the the passed value objects
			*
			* @input-required
			* @var needle {string}
			* @var haystack {object}
			*
			* @return {int}
			*/
			inObject: function(needle, haystack) {
				if(typeof haystack == 'object'){
					for(var i in haystack){
						if(haystack[i] === needle) return true;		
					}
				}
				else{
					var length = haystack.length;
					for(var i = 0; i < length; i++){
						if(typeof haystack == 'object'){
							alert(haystack[i]);	
						}
						if(haystack[i] === needle) return true;
					}
				}
				return false;
			}
		}
		//---------------------------------------------------------
		//init with config on first call
		_instance.set(config);
		//-------------------------------------------------------------
	return(_instance);
	};
	//init base
	return new base();
});



JM.SB_background = (function(){
	
}());
JM.SB_main = (function(){

    var homeTransform = function() {

    };

    var viewAllTransform = function() {
        $('.sold_out_tag').each(function(index, el) {
            $(this).closest('article').addClass('SB_st-soldout');
        });
        $('.sidebar').addClass('SB_st-visible');
    };

    var changeLink = function() {
        var $shoplink = $('.shop_link','body');
        $shoplink.attr('href', '/shop/all');
    };

    $('#sb_hide-so').on('click', function(event) {
        if ($('#hide_sold_out').is(':checked')) {
            chrome.storage.local.set({hide_so: 'false'});
        } else {
            chrome.storage.local.set({hide_so: 'true'});
        }
    });

    var callback = function() {

        chrome.storage.local.get('hide_so', function (result) {
            hideso = result.hide_so;
            if (hideso == 'true') {
                $('body').addClass('SB_so-hide');
            }
        });

        $('body').addClass('supreme_sb');

        if(window.location.href.indexOf("shop/shipping") > -1) {
            $('body').addClass('shipping');
        }

        if(window.location.href.indexOf("shop/terms") > -1) {
            $('body').addClass('terms');
        }

        if(window.location.href.indexOf("shop/faq") > -1) {
            $('body').addClass('faq');
        }

        if(window.location.href.indexOf("shop/sizing") > -1) {
            $('body').addClass('sizing');
        }

        if ($('body').hasClass('home')) {
            homeTransform();
        }

        if ($('body').hasClass('view-all')) {
            viewAllTransform();
        }

    };

    chrome.storage.local.get('hide_so', function (result) {
        hideso = result.hide_so;
        if (hideso == 'true') {
            $('#hide_sold_out').prop('checked', true);
        } else {
            $('.hide_sold_out').prop('checked', false);
        }
    });

    // chrome.storage.local.set({toggle: 'on'}, function() {
    //     chrome.browserAction.setIcon({path: "/htdocs/assets/img/ci/ext-icon.png"});
    //     // chrome.tabs.executeScript({file:"/htdocs/assets/js/content.js"});
    // });


    $(document).bind("DOMSubtreeModified", 'nav', function(event) {
        changeLink();
        viewAllTransform();

        chrome.storage.local.get('hide_so', function (result) {
            hideso = result.hide_so;
            if (hideso == 'true') {
                $('body').addClass('SB_so-hide');
            }
        });
    });


    if($(document).find("title").text().indexOf("Supreme") > -1) {
        callback();
    }

    return {
        callback: callback,
        changeLink: changeLink
    };


}());



