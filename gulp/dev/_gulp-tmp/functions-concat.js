/**
 * init breakpoint handler
 * @alias bpHandler
 * @namespace //@PROJECT_NAMESPACE
 * @return
 * @dependencies pubVars
 */

 JM.register('bpHandler',['pubVars'], function() {
     var callback = function() {

         var breakpoint = {};
         breakpoint.refreshValue = function() {
             this.value = window.getComputedStyle(
                 document.querySelector('body'), ':before'
             ).getPropertyValue('content').replace(/['"]+/g, '');
         };


         $(window).on('throttledresize', function() {
             breakpoint.refreshValue();
             if (breakpoint.value == 'desktop-l') {
                 JM.pubVars.set('dk-l',true);
             } else {
                 JM.pubVars.set('dk-l',false);
             }

             if (breakpoint.value == 'desktop') {
                 JM.pubVars.set('dk',true);
             } else {
                 JM.pubVars.set('dk',false);
             }

             if (breakpoint.value == 'tablet') {
                 JM.pubVars.set('tb',true);
             } else {
                 JM.pubVars.set('tb',false);
             }

             if (breakpoint.value == 'smartphone') {
                 JM.pubVars.set('sm',true);
             } else {
                 JM.pubVars.set('sm',false);
             }

         }).trigger('throttledresize');
     };

     callback();
     return {
         callback: callback
     };
 });

/**
 * init public variables
 * @alias pubVars
 * @namespace JM
 * @return
 */

JM.register('pubVars',[], function() {
    var config = {
        'isMobile': /(iPad|iPhone|iPod|Android)/g.test(navigator.userAgent),
        'sm': false,
        'tb': false,
        'dk': false,
        'dk-l': false
    };

    var set = function(varName, varData) {
        config[varName] = varData;
    };
    var get = function(varName) {
        return config[varName];
    };

    return {
        set: set,
        get: get
    };
});
