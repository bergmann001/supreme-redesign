/**
 * init public variables
 * @alias pubVars
 * @namespace JM
 * @return
 */

JM.register('pubVars',[], function() {
    var config = {
        'isMobile': /(iPad|iPhone|iPod|Android)/g.test(navigator.userAgent),
        'sm': false,
        'tb': false,
        'dk': false,
        'dk-l': false
    };

    var set = function(varName, varData) {
        config[varName] = varData;
    };
    var get = function(varName) {
        return config[varName];
    };

    return {
        set: set,
        get: get
    };
});
