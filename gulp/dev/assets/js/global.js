/*!
 * @preserve
 * @copyright:___________//@echo COPYRIGHT_INFO
 * @link:________________//@echo PROJECT_WEB_URL
 * @author:______________Jung von Matt/Neckar
 * @namespace:___________//@echo PROJECT_NAMESPACE
 * @function:____________global functions
 * @date:________________fullbuild: //@echo CHANGE_DATE
 */


//----------------------
//---PLUGINS.START

//@echo PLUGINS_JS_INCLUSION_LIST

//---PLUGINS.END
//----------------------



/**
 * Create global namespace
 *
 */
 var JMG = {};

/**
 * Use a closure to protect namespace and `$`
 */
JMG.getPrivateNs = (function($) {

    /**
     * Create private namespace
     *
     */
     var JM = {};


     //reqWaiters contains all the modules/functions that are waiting for their dependencies to be loaded / initiated
     JM.reqWaiters = {};
     //register registers a callback of which the result is bound to the namespace.
     //It can define dependencies of other functions/modules.
     //@example register('test', ['test-other'], callback) will load test but wait for test-other to be loaded (test-other is the dependency in this case)
     JM.register = function(namespace, dependencies, callback) {
       var bMeetsAllDependencies = true;
       for (var i in dependencies) {
         if (typeof JM[dependencies[i]] === 'undefined' || !(dependencies[i] in JM)) {
           bMeetsAllDependencies = false;
           JM.reqWaiters[namespace] = {"namespace": namespace, "dependencies": dependencies, "callback": callback};
           break;
         }
       }

       if (bMeetsAllDependencies) {
         JM[namespace] = callback();
         delete JM.reqWaiters[namespace];

         for (var i in JM.reqWaiters) {
           JM.register(JM.reqWaiters[i].namespace, JM.reqWaiters[i].dependencies, JM.reqWaiters[i].callback);
         }
       }
     };

     $(window).load(function(){
         JM.register(':load', [], function(){
            return true;
         });
     });


     //----------------------
     //---OBJECTS.START

     //= include ../../_gulp-tmp/objects-concat.js

     //---OBJECTS.END
     //----------------------


    //----------------------
    //---MODULES.START

    //= include ../../_gulp-tmp/modules-concat.js

    //---MODULES.END
    //----------------------


     return function() {
        return JM;
     };
 }(jQuery));
