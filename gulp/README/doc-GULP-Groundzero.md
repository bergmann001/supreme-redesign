


JVM Groundzero v2.6g
====================

[TOC]


# Gulp Commands
## Informal Commands

- `gulp check`  will show a configuration statistic
- `gulp check-deep`  will show more detailed configuration
- `gulp list-jsplugins`  will list you all the global available js plugins (not yet implemented!)

## Build/Watch Commands

- `gulp`  will start the default task - which is watching
- `gulp fullbuild`  will create the complete app/ structure with all files (also images! this could take time - potentially)
- `gulp clean` will delete the app/ directory for you to make a fresh build
- `gulp add-module` will give you a prompt to add a module to the project - automagically


# Build Configuration
Any project-specific configuration is handled inside the <strong><i class="icon-folder-open"></i>project-config</strong> directory.

	...
	// _EXAMPLE BUILD FILE
	var jsplugins_registry 
		= require('./js-plugins-registry.js')();
		
	module.exports =
	    BuildConfig
	        .devSource('./dev')
	        .buildAt('./app')
	            .assetsAt('assets')
	                .imagesAt('images')
	        .fileVariables([
		        {
			        search: 'bla',
			        replace: 'blubbs'
				}
			])
	        .localVar('PROJECT_NAMESPACE', 'JM_')
	        .localVar('AUTO_RETINA_IMAGES', true) 
	        .localVar('CREATE_INLINEICONS_CSS', true)
	        .jsplugins() 
	            .useJsRegistry(jsplugins_registry)
	            .add('fastclick')
	            .add('jquery-2', true) 
	            .add('respondjs', true, 'vendor') 
	        .end();

## Variables explained:
Any var defined by `localVar(..)` is only visible inside the BuildConfig object but *not* compiled into any file. To provide vars to files that will be compiled you need to use `fileVariables(..)` as seen above. 

### Output file variables
You can use vars defined by `fileVariables(..)` in any of your source files. 

- In .js-files: `//@echo VARIABLENAME` or `/** @echo VARIABLENAME **/`
- In .sass-files: `//@echo VARIABLENAME` or `/** @echo VARIABLENAME **/`
- In .html-files: `<!-- @echo VARIABLENAME -->`

## Defining JS-Plugins (project dependencies):
Any JS that is required for the project can be defined by using the `add()`-Function bound to `jsplugins()`. 

The signature is as follows:

	add(
		string pluginName, 
		[bool compileStandalone], 
		[string standaloneTargetDir]
		)


------------------
# Structure and Conventions
## Directory Structure
.dev/
<small>
├─ <font color="#cab4bb">_gulp-ext/</font>
├─ <font color="#cab4bb">_gulp-tmp/</font></small>
├─ global/
&nbsp; &nbsp; &nbsp; &nbsp;└─ js/
&nbsp; &nbsp; &nbsp; &nbsp;├─└─ plugins/
&nbsp; &nbsp; &nbsp; &nbsp;└─ sass/
&nbsp; &nbsp; &nbsp; &nbsp;└─ templates/
&nbsp; &nbsp; &nbsp; &nbsp;├─└─ global_modules/
&nbsp; &nbsp; &nbsp; &nbsp;├─└─ includes/
├─ img/
├─└─ bg/
├─└─ ci/
├─└─ etc/
├─└─ icons/
├─└─ scenes/
├─ modules/


### Project Images
#### Where to put the image?
- Is it a photo or a senseful big illustration? 
	 &nbsp;&nbsp; **scenes/**
- Does it belong to the CI of the client? 
	 &nbsp;&nbsp; **ci/**
- Is it an icon? 
	 &nbsp;&nbsp; **icons/**
- Is it a CSS bg-image that is not a scene nor a ci-type or icon? 
	 &nbsp;&nbsp; **bg/**
- Is it something that is none of the mentioned above? 
	 &nbsp;&nbsp; **etc/**


### Project JS Plugins 
The <strong><i class="icon-folder-open"></i>global/js/plugins</strong> contains any  JS file that *can* be used within the project. 

> The mentioned directory is not project-specific but rather contains **all** libs like JQuery, matchMedia, Hammer, fastclick, etc. and is meant to grow infinitely.

#### Add new JS to global registry

1. Make sure to use the **NON-minified** (development) version of the JS-file!
2. Go to the directory mentioned above and create a directory for your plugin (e.g.: <i class="icon-folder-open">global/js/plugins/<strong>plugin-name</strong></i>)
3. Put your JS file into that directory 
(e.g.: <i class="icon-folder-open">global/js/plugins/plugin-name/<strong>plugin-name.js</strong></i>)
4. Open <i class="icon-folder-open">project-config/js-plugins-registry.js</i>, add your entry - finished!

> In most cases you don't have to do this anyway because most of the libraries will already be available!


### Project Templates (Layouts)
A Frontend project consists of multiple Layouts (Default Page Layout, Sub Site 1 Layout, Sub Site 2 Layout, ...). Any Layout Template consists of a layout-specific HTML structure with parts of modules inside and is put inside the <strong><i class="icon-folder-open"></i>global/templates</strong> directory.

Naming convention: Any Template begins with `TPL_`. For instance creating the default page should be named as follows: `TPL_index.html` and if you want to create a Wiki Layout it would make sense to use `TPL_wiki.html`.

### Project Modules
Modules are meant to modularize the whole project to make it as independent and reusable as possible. A module can provide module-specific JS, HTML and SASS to the project respectively the project.

Any module directory structure should look as follows <small>(you do not have to create it by yourself)</small>:
>.dev/
├─ .modules/
├─└─ your_module_name/
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├─└─ js/
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├─└─ sass/
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ├─└─ templates/

#### Create new module
**NEVER** create modules by hand. Always use the gulp-task!
Open the terminal and enter `gulp add-module` 
> The gulp-task will ask you for a senseful name of the module you want to create and then build the corresponding files for you :)

##### Module JavaScript
The `your_module_name/js/_MOD_your_module_name.js` file is automatically compiled into the main JS-file. Just start coding!

#### Module SASS
The `your_module_name/sass/_MOD_your_module_name.scss` file is for any styles your module uses. Unfortunately - in favour of sourcemaps generation - it is **NOT automatically compiled** into the main SASS file. So in the main SASS file use 

	@import '
	..../your_module_name/sass/MOD_your_module_name.scss';

#### Module HTML
Any templates build inside the module directory can be easily reused anywhere in any other module *AND* template:

<i class="icon-file"></i> global/templates/TPL_index.html:
	
	<html>
		<use src="headermodule/headerbox" data-context="{}" />
		<body>
			<div class="JM_grid JM_grid-cont-3-3">
				<div class="JM_col">
				  <use src="othermodule/textbox" data-context="{'heading': 'Textbox 1'}" />
				</div>
				<div class="JM_col">
				  <use src="othermodule/textbox" data-context="{'heading': 'Textbox 2 :)'}" />
				</div>
			</div>
		</body>
	</html>

##### And how does it work?
By convention!

    <use src="path/relative/to/modulesdirectory/module/filename" data-context="{'variable': 'value'}" />


#### Special case: A module that only contains JS
Obviously, there are cases where you just want to create a js module without SASS or HTML. In that case you clarify it by prefixing the module with `js`. Example: `js-headermodule/`. 

> This will not change anything but make it easier to distinguish the modules!



