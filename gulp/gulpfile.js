var gulp = require('gulp'),
prompt = require('prompt'),
fs = require('fs'),
mkdirp = require('mkdirp'),
clean = require('gulp-clean'),
include = require('gulp-include'),
preprocess = require('gulp-preprocess'),
notifyWrapper = require('gulp-notify-wrapper'),
plumber = require('gulp-plumber'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
base64 = require('gulp-base64'),
rename = require('gulp-rename'),
sass = require('gulp-sass'),
cssminify = require('gulp-minify-css'),
autoprefixer = require('gulp-autoprefixer'),
wrap = require('gulp-wrap'),
flatten = require('gulp-flatten'),
javascriptobfuscator = require('gulp-javascriptobfuscator'),
strip = require('gulp-strip-comments'),
filefilter = require('gulp-filter'),
graphicsmagick = require('gulp-gm'),
sourcemaps = require('gulp-sourcemaps'),
BuildConfig = require('./project-config/project-config.js');

BuildConfig.verify();

var PATH_tmp                      = BuildConfig.getDevPath('_gulp-tmp');
var PATH_JM_globaljs_dir          = BuildConfig.getDevPath('assets/js');
var PATH_JM_globaljs              = PATH_JM_globaljs_dir+'/'+BuildConfig.localVar('GLOBAL_JS_NAME');
var PATH_JM_globalSass            = BuildConfig.getDevPath('assets/sass/'+BuildConfig.localVar('GLOBAL_SASS_NAME'));
var PATH_JM_globalSass_enhanced   = BuildConfig.getDevPath('assets/sass/'+BuildConfig.localVar('GLOBAL_SASS_ENHANCED_NAME'));

var INGLOBALSASS_path_map = {
  'MOD': '../../'+BuildConfig._paths.dev.subPath_modulesSrc,
  'OBJ': '../../'+BuildConfig._paths.dev.subPath_objectsSrc
};


var PATH_customAssets = [
    BuildConfig.getDevAssetsPath('**/*'),
    '!'+BuildConfig.getDevAssetsPath('img'),
    '!'+BuildConfig.getDevAssetsPath('img/*'),
    '!'+BuildConfig.getDevAssetsPath('img/**/*'),
    '!'+BuildConfig.getDevAssetsPath('sass'),
    '!'+BuildConfig.getDevAssetsPath('sass/*'),
    '!'+BuildConfig.getDevAssetsPath('sass/**/*'),
    '!'+BuildConfig.getDevAssetsPath('js'),
    '!'+BuildConfig.getDevAssetsPath('js/*'),
    '!'+BuildConfig.getDevAssetsPath('js/**/*')
];

var PATH_JM_globalTpls  = BuildConfig.getDevPath('templates/*.html');
var PATH_JM_global_subTpls  = BuildConfig.getDevPath('templates/**/*.html');

var PATH_JM_modules     = {
  js: [BuildConfig.getDevPath('modules/*/js/_MOD*.js')],
  tplSass: BuildConfig.getDevPath('modules/*/sass/*.scss'),
  templates: BuildConfig.getDevPath('modules/*/templates/*.html')
};
var PATH_JM_objects     = {
  js: [BuildConfig.getDevPath('objects/*/js/_OBJ*.js')],
  tplSass: BuildConfig.getDevPath('objects/*/sass/*.scss'),
  templates: BuildConfig.getDevPath('objects/*/templates/*.html')
};

var PATH_devImages = {
  rasteredImg: [BuildConfig.getDevSrcImgPath()+'/**/*.@(jpg|jpeg|png)'],
  icons: [BuildConfig.getDevSrcImgPath()+'/icons/*.@(svg|png)'],
  svg: [BuildConfig.getDevSrcImgPath()+'/**/*.svg']
};

var PreprocessorContext = {
  withJsPlugins: BuildConfig.createPreprocessContext({includeJsPlugins: true, noNgExpressionReplacement: !!BuildConfig.localVar('NO_NG_EXPRESSION_REPLACEMENT')}),
  withoutJsPlugins: BuildConfig.createPreprocessContext({includeJsPlugins: false, noNgExpressionReplacement: !!BuildConfig.localVar('NO_NG_EXPRESSION_REPLACEMENT')})
};







gulp.task('clean', function(){
  return gulp.src([BuildConfig.getBuildPath(), PATH_tmp+'/*'], {read: false})
  .pipe(clean());
});



/**
Image Processing > START
-----------------------------
**/
gulp.task('prep-images-retina', function(){
  var filter2x = filefilter(['**/*2x.@(jpg|jpeg|png)']);

  var rasteredImgPipe = gulp.src(PATH_devImages.rasteredImg);
  if (BuildConfig.localVar('AUTO_RETINA_IMAGES')) {
    return rasteredImgPipe
    .pipe(filter2x)
    .pipe(graphicsmagick(function(gmfile, done){
      gmfile.size(function(err, size){
        done(null, gmfile.resize(0.5 * size.width, 0.5 * size.height));
      })
    }))
    .pipe(rename(function(filepath) {
      filepath.basename = filepath.basename.substring(0,filepath.basename.length - 3);
    }))
    .pipe(gulp.dest(BuildConfig.getTargetImgPath()));
  } else {
    return true;
  }

});

gulp.task('prep-virtual-tmp-icons-css', function(){

  if (BuildConfig.localVar('CREATE_INLINEICONS_CSS')) {
    var cssClassPrepend = '.'+BuildConfig.localVar('PROJECT_NAMESPACE')+'icon-';
    return gulp.src(PATH_devImages.icons)
    .pipe(wrap(function(data){
      //var fileNameOnly = data.file.
      var fileName = data.file.path.substr(data.file.base.length);
      var fileExt  = fileName.substring(fileName.length-3);
      var fileNameNoExt = fileName.substring(0, fileName.length - 4);
      var cssClassForFile = cssClassPrepend+fileNameNoExt.toLowerCase();

      if (fileExt=='svg') {
        cssClassForFile += '-svg';
      }

      //attention: using ../ because the base64() gulp plugin requires relative paths!
      return cssClassForFile + '{background-image:url("../'+BuildConfig.getDevImgDirname()+'/icons/'+fileName+'")'+'}';
    }))
    .pipe(concat('icons.css'))
    .pipe(gulp.dest(PATH_tmp));
  }

  return true;
});

gulp.task('prep-images-icons', ['prep-virtual-tmp-icons-css'], function(){

  if (BuildConfig.localVar('CREATE_INLINEICONS_CSS')) {
    var iconsTmpCss = gulp.src(PATH_tmp+'/icons.css');

    //maybe: we could also generate an icons.css in "prep-virtual-tmp-icons-css" which really references the files!
    //.. that way we could use that icons.css as an icons-fallback.css
    //.. think about it :)

    iconsTmpCss
    .pipe(base64())
    .pipe(gulp.dest(BuildConfig.getTargetCssPath()));
  }

  return true;
});

gulp.task('prep-images', ['prep-images-retina', 'prep-images-icons'], function(){
  //copy rastered images
  gulp.src(PATH_devImages.rasteredImg)
  .pipe(gulp.dest(BuildConfig.getTargetImgPath()));


  //copy svg images
  gulp.src(PATH_devImages.svg)
  .pipe(gulp.dest(BuildConfig.getTargetImgPath()));
});
/**
-----------------------------
Image Processing > END
**/



gulp.task('list-jsplugins', function(){

});


gulp.task('generate-templates', function(){
  //we dont want to compile the "local" templates: return gulp.src([PATH_JM_globalTpls, PATH_JM_modules.tpls])
  //var preprocessingContext = BuildConfig.createPreprocessContext({includeTplUsables: true});

  return gulp.src([PATH_JM_globalTpls])
  .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
  .pipe(include())
  .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
  .pipe(notifyWrapper.compile.success())
  .pipe(flatten())
  .pipe(gulp.dest(BuildConfig.getTargetTemplatesPath()));
});



gulp.task('compile-css', function() {


  return gulp.src([PATH_JM_globalSass, PATH_JM_globalSass_enhanced])
  //not needed ATM: .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
  .pipe(rename({
    prefix: BuildConfig.localVar('PROJECT_NAMESPACE')
  }))
  // .pipe(plumber(notifyWrapper.compile.error()))
  .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
  .pipe(sourcemaps.init())
  .pipe(sass({
    includePaths: require('node-bourbon').includePaths
  }))
  .pipe(sourcemaps.write('./', {sourceRoot: './'}))
  .pipe(notifyWrapper.compile.success())
  .pipe(gulp.dest(BuildConfig.getTargetCssPath()));


  //PATH_JM_globalSass_enhanced
});

gulp.task('create-css', ['compile-css'], function() {
  return gulp.src([
    BuildConfig.getTargetCssPath()+'/*.css',
    '!'+BuildConfig.getTargetCssPath()+'/*.min.css'
  ])
  .pipe(cssminify({
    processImport: false,
    noRebase: false,
    noAdvanced: true
  }))
  .pipe(rename({
    suffix: '.min'
  }))
  //.pipe(notifyWrapper.compile.success())
  .pipe(gulp.dest(BuildConfig.getTargetCssPath()));
});



var task_createConcattedJs = function(prefix, src) {
  return function(){
    return gulp.src(src)
    .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
    .pipe(plumber(notifyWrapper.compile.error()))
    .pipe(include())
    .pipe(preprocess(PreprocessorContext.withoutJsPlugins))
    .pipe(concat(prefix+'-concat.js'))
    .pipe(gulp.dest(PATH_tmp));
  };
};





gulp.task('create-tmp-objects-js', task_createConcattedJs('objects',PATH_JM_objects.js));
gulp.task('create-tmp-modules-js', task_createConcattedJs('modules',PATH_JM_modules.js));
gulp.task('create-tmp-functions-js', task_createConcattedJs('functions',BuildConfig.getDevAssetsPath('js/functions/_FN*.js')));

gulp.task('create-js', ['create-tmp-objects-js','create-tmp-modules-js','create-tmp-functions-js'], function(){
  gulp.src(PATH_JM_globaljs)
  .pipe(rename({
    prefix: BuildConfig.localVar('PROJECT_NAMESPACE')
  }))
  .pipe(plumber(notifyWrapper.compile.error()))
  .pipe(preprocess(PreprocessorContext.withJsPlugins))
  .pipe(include())
  .pipe(notifyWrapper.compile.success())
  // .pipe(gulp.dest(BuildConfig.getTargetJsPath()))
  .pipe(strip())
  .pipe(javascriptobfuscator({
        encodeString: true,
        encodeNumber: true,
        replaceNames: true,
        moveString: true,
        exclusions: ["^_get_", "^_set_", "^_mtd_"]
    }))
  // .pipe(uglify({
  //   mangle: false,
  //   preserveComments: 'some',
  //   compress: {
  //     dead_code: false,
  //     unused: false
  //   }
  // }))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(BuildConfig.getTargetJsPath()));


  var standaloneJsPlugins = BuildConfig.getStandaloneJsPlugins();
  gulp.src(standaloneJsPlugins.paths)
  .pipe(gulp.dest(standaloneJsPlugins.dest))
  .pipe(uglify({
    mangle: false,
    preserveComments: 'some',
    compress: {
      dead_code: false,
      unused: false
    }
  }))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest(function(v){
    return v.base;
  }));
});

gulp.task('copy-custom-assets', function(){
  gulp.src(PATH_customAssets)
  .pipe(gulp.dest(BuildConfig.getTargetAssetsPath()));
});

gulp.task('fullbuild', ['copy-custom-assets', 'create-js', 'create-css', 'prep-images', 'generate-templates'],  function(){

});

gulp.task('release', function(){
  gulp.src(PATH_customAssets)
  .pipe(gulp.dest(BuildConfig.getTargetAssetsPath()));
});


gulp.task('fb', ['fullbuild'],  function(){
    
});

var gulpTask_addComponent = function(componentPrefix, componentPath) {
  return function(done){
    prompt.start();
    var requestPromptData = [
      {
        description: 'Enter module name (only a-z/_-)',
        type: 'string',
        pattern: /[a-z]+([\_a-z])+([a-z\-])*/,
        required: true,
        name: 'projectname'
      }
    ];
    prompt.get(requestPromptData, function(err, result){
      if (!err) {
        var modulesDir = BuildConfig.getDevPath(componentPath+'/'+result.projectname);
        mkdirp(modulesDir+'/js', function(jsDirErr){
          mkdirp(modulesDir+'/sass', function(sassDirErr) {
            if (sassDirErr) {
              return;
            }


            var modulesSassRel = 'sass/_'+componentPrefix+'_'+result.projectname+'%s.scss';

            var moduleSassRel_name = modulesSassRel.replace('%s','');
            fs.writeFile(modulesDir+'/'+moduleSassRel_name,
            "."+BuildConfig.localVar('PROJECT_NAMESPACE')+componentPrefix+"_"+result.projectname+" {"
            +"\n \n"
            +"}\n\n"
            +"", null, (function(_modSassRel){
              return function(err){
                if (!err) {
                  //file was written. add it to the JM_global

                  fs.readFile(PATH_JM_globalSass, 'utf8', (function(_sassPath){
                    return function(err,data){
                      if (err)  {
                        return console.error(err);
                      }

                      var res = data.replace(
                        '//@ADD_'+componentPrefix+'_HERE',
                        "//@ADD_"+componentPrefix+"_HERE\n\t@import '"
                        +INGLOBALSASS_path_map[componentPrefix]+'/'+result.projectname+'/'
                        +_modSassRel.replace('_'+componentPrefix+'_',componentPrefix+'_')+"';");
                        fs.writeFile(_sassPath, res);
                      };
                    }(PATH_JM_globalSass)));
                  }
                };
              }(moduleSassRel_name)));

            var breakpointsEnhanced = ['tb','dk','dk-l'];
            var sassStr = '';
            for (var i in breakpointsEnhanced) {
              sassStr += '@include breakpoint('+breakpointsEnhanced[i]+') {' + "\n"
                +"\t"+'.'+BuildConfig.localVar('PROJECT_NAMESPACE')+componentPrefix+"_"+result.projectname+" {"
                +"\t\t\n \n"
                +"\t}\n"
              +'}'+"\n\n";
            }

            //Create enhanced SASS file/module (enhanced = bigger breakpoints than $device-first)
            moduleSassRel_name = modulesSassRel.replace('%s', '-enhanced');
            fs.writeFile(modulesDir+'/'+moduleSassRel_name,sassStr, null, (function(_modSassRel){
              return function(err){
                if (!err) {
                  //file was written. add it to the JM_global

                  var sassPath = PATH_JM_globalSass.split('.scss')[0]+'-enhanced.scss';
                  fs.readFile(sassPath, 'utf8', (function(_sassPath){
                    return function(err,data){
                      if (err)  {
                        return console.error(err);
                      }

                      var res = data.replace(
                        '//@ADD_'+componentPrefix+'_HERE',
                        "//@ADD_"+componentPrefix+"_HERE\n\t@import '"
                        +INGLOBALSASS_path_map[componentPrefix]+'/'+result.projectname+'/'
                        +_modSassRel.replace('_'+componentPrefix+'_',componentPrefix+'_')+"';");
                        fs.writeFile(_sassPath, res);
                      };
                    }(sassPath)));
                  }
                };
              }(moduleSassRel_name)));

              mkdirp(modulesDir+'/templates', function() {
                fs.writeFile(
                  modulesDir+'/templates/_'+componentPrefix+'_'+result.projectname+'.html',
                  '<div class="'+BuildConfig.localVar('PROJECT_NAMESPACE')+componentPrefix+"_"+result.projectname+'">'
                  +'\n \n'
                  +'</div>'
                );
              });
            });


            var JS_NAMESPACE = BuildConfig.localVar('PROJECT_NAMESPACE');
            JS_NAMESPACE = JS_NAMESPACE.substr(0,JS_NAMESPACE.length-1)+'.';
            if (jsDirErr) {return;}
            fs.writeFile(
              modulesDir+'/js/_'+componentPrefix+'_'+result.projectname+'.js',
              JS_NAMESPACE+result.projectname.replace(/\-/g,"_")
              +" = (function(){\n\t\n}());");
            });
          }
          done();
        });
      }
};



gulp.task('add-module', gulpTask_addComponent('MOD','modules'));
gulp.task('add-object', gulpTask_addComponent('OBJ', 'objects'));

gulp.task('check', function(){
  BuildConfig.print();
});

gulp.task('check-deep', function(){
  BuildConfig.print(true);
  //TODO: Idea: Check also the folder structure of the plugins and check if there is something that is NOT in the js-registry
});

gulp.task('default', function(){
  BuildConfig.verify();
  gulp.watch([PATH_JM_globalSass, PATH_JM_modules.tplSass, PATH_JM_objects.tplSass], ['create-css']);
  gulp.watch([PATH_JM_globaljs_dir+'/**/*.js'].concat(PATH_JM_modules.js).concat(PATH_JM_objects.js), ['create-js']);
  gulp.watch([PATH_JM_globalTpls, PATH_JM_modules.templates, PATH_JM_objects.templates], ['generate-templates']);
});

gulp.task('fbw', ['fullbuild'],  function(){
  BuildConfig.verify();
  gulp.watch([PATH_JM_globalSass, PATH_JM_modules.tplSass, PATH_JM_objects.tplSass], ['create-css']);
  gulp.watch([PATH_JM_globaljs_dir+'/**/*.js'].concat(PATH_JM_modules.js).concat(PATH_JM_objects.js), ['create-js']);
  gulp.watch([PATH_JM_globalTpls, PATH_JM_modules.templates, PATH_JM_objects.templates], ['generate-templates']); 
});
